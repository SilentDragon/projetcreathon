var markers = []

function createMarker(c, coords) {
  var id
  if (markers.length < 1) id = 0
  else id = markers[markers.length - 1]._id + 1
  var popupContent =
    '<button onclick="clearMarker(' + id + ')">Clear Marker</button>';
  switch (c) {
    case 'p'://icone parking
        myMarker = L.marker(coords, {icon: parkingIcon}, {
          draggable: false
        });
        parkings.addLayer(myMarker);
      break;
    case "d":// default
        myMarker = L.marker(coords, {icon: defaultIcon}, {
          draggable: false
        });
        autres.addLayer(myMarker);
      break;
    case "v":// Vélos
    myMarker = L.marker(coords, {icon: veloIcon}, {
      draggable: false
    });
    velos.addLayer(myMarker);
      break;
    case "b"://bus
    myMarker = L.marker(coords, {icon: busIcon}, {
      draggable: false
    });
    bus.addLayer(myMarker);
      break;
    case "t"://tram
    myMarker = L.marker(coords, {icon: tramIcon}, {
      draggable: false
    });
    trams.addLayer(myMarker)
      break;
    default:
      return;
  }
    //************************************ 
  myMarker._id = id
  var myPopup = myMarker.bindPopup(popupContent, {
    closeButton: false
  });
  markers.push(myMarker)
}

function clearMarker(id) {
	console.log(markers)
  var new_markers = []
  markers.forEach(function(marker) {
    if (marker._id == id) map.removeLayer(marker)
    else new_markers.push(marker)
  })
  markers = new_markers
}
/* ********************************** */
var parkings = L.layerGroup();
var velos = L.layerGroup();
var autres = L.layerGroup();
var trams = L.layerGroup();
var bus = L.layerGroup();
var shapes = L.layerGroup();

var osm = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 19,
    attribution: '© OpenStreetMap'
});

var watercolor = L.tileLayer('https://stamen-tiles.a.ssl.fastly.net/watercolor/{z}/{x}/{y}.png', {id: 'MapID', tileSize: 512, zoomOffset: -1, attribution: '© Stamen'});

var matrix = L.tileLayer('https://{s}.tile.jawg.io/jawg-matrix/{z}/{x}/{y}{r}.png?access-token=7HnFPdM8LqfGL1RmOvLVG2J61xUzznKd19MYzMWf7EjvuTHPmnT9bje6c0WawXxK', {
	attribution: '<a href="http://jawg.io" title="Tiles Courtesy of Jawg Maps" target="_blank">&copy; <b>Jawg</b>Maps</a> &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
	minZoom: 0,
	maxZoom: 22,
	subdomains: 'abcd',
	accessToken: '7HnFPdM8LqfGL1RmOvLVG2J61xUzznKd19MYzMWf7EjvuTHPmnT9bje6c0WawXxK'
});

var organique = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
	attribution: '© Esri, Arcgis'
});

var map = L.map('map', {
    center: [50.357113, 3.518332],
    zoom: 10,
    keyboard : false, 
    layers: [osm, autres]
});

var baseMaps = {
    "Routes": osm,
    "Watercolor" : watercolor,
    "Matrix " : matrix,
    "Organique" : organique
};

var overlayMaps = {
    "Parking" : parkings,
    "Vélo" : velos,
    "Bus" : bus,
    "Tram" : trams,
    "Défaut": autres,
    "Formes" : shapes
};

var layerControl = L.control.layers(baseMaps, overlayMaps).addTo(map);

parkings.addTo(map);
velos.addTo(map);
autres.addTo(map);
trams.addTo(map);
bus.addTo(map);
shapes.addTo(map);

var mode = 'd'

var polygonPoints = [];
var circlePoints = [];
function onMapClick(e) {
  if(mode=='c')
  {
    if(circlePoints.length==0){circlePoints.push(e.latlng); return ;}
    else {
      circlePoints.push(e.latlng);
      makeCircle();
    }
  }
  if(mode=='z')polygonPoints.push(e.latlng);
  else createMarker(mode,e.latlng);
}

function makeCircle()
{ 
  var distance = distanceMetres(circlePoints[0], circlePoints[1]);
  var radius = distance/2;
  console.log("rayon : "+ radius);
  var center = [(circlePoints[0].lat+circlePoints[1].lat)/2,(circlePoints[0].lng+circlePoints[1].lng)/2]
  console.log("Point A : "+ circlePoints[0]);
  console.log("Point B : "+ circlePoints[1]);
  console.log("centre : " + center);
  var circle = L.circle(center, {radius: radius}).addTo(map);
      shapes.addLayer(circle);
      circlePoints.length=0; //Pour reset les points maintenant que le polygone est tracé
      return;
}

function deleteMarker (e){
    console.log(e.target);
      e.target.remove();
}

function onKeyDown(e) {
  if(mode =='z')
  {
    if(e.key=='z')
    {
      var polygon = L.polygon(polygonPoints, {color: 'red'}).addTo(map);
      shapes.addLayer(polygon);
      polygonPoints.length=0; //Pour reset les points maintenant que le polygone est tracé
      return;
    }
    else
    {
      polygonPoints.length=0; //On laisse tomber les points déjà tracés si l'utilisateur appuie sur une autre touche
    }
  }
    switch (e.key) {
      case 'p'://icone parking
          mode = 'p';
        break;
      case "d":// default
          mode = 'd';
      break;
      case "v":// Vélos
          mode = 'v';
      break;
      case "b"://bus
          mode = 'b';    
      break;
      case "t"://tram
          mode = 't';
      break;
      case "z"://zone (polygone)
          mode = 'z';
      break;
      case "c"://zone (cercle)
          mode = 'c';
      break;
      default:
        alert("Touche non prise en charge");
        console.log("touche non prise en charge");
        return;
    }
    console.log("on est passé en mode " + mode);
}

function newMarker(c,data){
    console.log("newMarker avec c =" + c)
    switch (c) {
        case 'p'://icone parking
            console.log(data);
            var marker = L.marker(data, {icon: parkingIcon}, {draggable: true});
            parkings.addLayer(marker);
          break;
        case "d":// default
            console.log(data);
            var marker = L.marker(data, {icon: defaultIcon});
            autres.addLayer(marker);
          break;
        case "v":// Vélos
        console.log(data);
            var marker = L.marker(data, {icon: veloIcon});
            velos.addLayer(marker);
          break;
        case "b"://bus
        console.log(data);
            var marker = L.marker(data, {icon: busIcon});
            bus.addLayer(marker);
          break;
        case "t"://tram
        console.log(data);
            var marker = L.marker(data, {icon: tramIcon}).addTo(map);
            trams.addLayer(marker);
          break;
        case "s"://suppression
          console.log(data);
              L.marker(data, {icon: tramIcon}).addTo(map);
          break;
        default:
          return;
      }
}

map.on('click', onMapClick);

L.easyPrint({
	title: 'print',
	position: 'bottomright',
	sizeModes: ['A4Landscape']
}).addTo(map);

document.addEventListener('keydown', onKeyDown);

var greenIcon = L.icon({
  iconUrl: '../assets/leaf-green.png',
  shadowUrl: '../assets/leaf-shadow.png',

  iconSize:     [38, 95], // size of the icon
  shadowSize:   [50, 64], // size of the shadow
  iconAnchor:   [22, 94], // point of the icon which will correspond to marker's location
  shadowAnchor: [4, 62],  // the same for the shadow
  popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
});

var parkingIcon = L.icon({
  iconUrl: '../assets/parking.png',
  iconSize:     [38, 38], // size of the icon
  iconAnchor:   [0,38], // point of the icon which will correspond to marker's location
  popupAnchor:  [20, -30] // point from which the popup should open relative to the iconAnchor
});

var tramIcon = L.icon({
iconUrl: '../assets/tram.png',
iconSize:     [38, 38], // size of the icon
iconAnchor:   [0,38], // point of the icon which will correspond to marker's location
popupAnchor:  [20, -30] // point from which the popup should open relative to the iconAnchor
});

var defaultIcon = L.icon({
iconUrl: '../assets/iconeDefaut.png',
iconSize:     [38, 60], // size of the icon
  iconAnchor:   [22, 60], // point of the icon which will correspond to marker's location
  popupAnchor:  [-3, -50] // point from which the popup should open relative to the iconAnchor
});

var veloIcon = L.icon({
iconUrl: '../assets/velo.png',
iconSize:     [38, 38], // size of the icon
iconAnchor:   [0,38], // point of the icon which will correspond to marker's location
popupAnchor:  [20, -30] // point from which the popup should open relative to the iconAnchor
});

var busIcon = L.icon({
iconUrl: '../assets/bus.png',
iconSize:     [38, 38], // size of the icon
iconAnchor:   [0,38], // point of the icon which will correspond to marker's location
popupAnchor:  [20, -30] // point from which the popup should open relative to the iconAnchor
});

function distanceMetres(p1,p2)
{
lon1 =  p1.lng * Math.PI / 180;
lon2 = p2.lng * Math.PI / 180;
lat1 = p1.lat * Math.PI / 180;
lat2 = p2.lat * Math.PI / 180;

// Haversine formula
let dlon = lon2 - lon1;
let dlat = lat2 - lat1;
let a = Math.pow(Math.sin(dlat / 2), 2)
+ Math.cos(lat1) * Math.cos(lat2)
* Math.pow(Math.sin(dlon / 2),2);

let c = 2 * Math.asin(Math.sqrt(a));

// Radius of earth in kilometers. Use 3956
// for miles
let r = 6371*1000;

// calculate the result
return(c * r);
}