var markers = []

function createMarker(c, coords) {
  var id
  if (markers.length < 1) id = 0
  else id = markers[markers.length - 1]._id + 1
  var popupContent =
    '<p>Some Infomation</p></br>' +
    '<p>test</p></br>' +
    '<button onclick="clearMarker(' + id + ')">Clear Marker</button>';
    //Partie à custom en fonction de c, le mode
    //************************************* 
  myMarker = L.marker(coords, {icon: parkingIcon}, {
    draggable: false
  });
    //************************************ 
  myMarker._id = id
  var myPopup = myMarker.bindPopup(popupContent, {
    closeButton: false
  });
  map.addLayer(myMarker)
  markers.push(myMarker)
}

function clearMarker(id) {
	console.log(markers)
  var new_markers = []
  markers.forEach(function(marker) {
    if (marker._id == id) map.removeLayer(marker)
    else new_markers.push(marker)
  })
  markers = new_markers
}


var map = L.map("map").setView([50.357113, 3.518332], 15);

L.easyPrint({
	title: 'My awesome print button',
	position: 'bottomright',
	sizeModes: ['A4Portrait', 'A4Landscape']
}).addTo(map);

var mode = 'd'
var deleteBool = false;
var polygonPoints = [];



function onMapClick(e) {
    if(mode=='z')polygonPoints.push(e.latlng);
    else createMarker(mode,e.latlng);
    
}




function deleteMarker (e){
    console.log(e.target);
      e.target.remove();
} 

function onKeyDown(e) {
  if(mode =='z')
  {
    if(e.key=='z')
    {
      var polygon = L.polygon(polygonPoints, {color: 'red'}).addTo(map);
      map.fitBounds(polygon.getBounds()); //Pour recentrer la carte sur le polygone
      polygonPoints.length=0; //Pour reset les points maintenant que le polygone est tracé
      return;
    }
    else
    {
      polygonPoints.length=0; //On laisse tomber les points déjà tracés si l'utilisateur appuie sur une autre touche
    }
  }
    switch (e.key) {
      case 'p'://icone parking
          mode = 'p';
        break;
      case "d":// default
          mode = 'd';
      break;
      case "v":// Vélos
          mode = 'v';
      break;
      case "b"://bus
          mode = 'b';    
      break;
      case "t"://tram
          mode = 't';
      break;
      case "z"://zone (polygone)
          mode = 'z';
      break;
      default:
        alert("Touche non prise en charge");
        console.log("touche non prise en charge");
        return;
    }
    console.log("on est passé en mode " + mode);
}

function newMarker(c,data){
    console.log("newMarker avec c =" + c)
    switch (c) {
        case 'p'://icone parking
            console.log(data);
            L.marker(data, {icon: parkingIcon}, {draggable: true}).addTo(map);
          break;
        case "d":// default
        console.log(data);
            L.marker(data, {icon: defaultIcon}).addTo(map);
          break;
        case "v":// Vélos
        console.log(data);
            L.marker(data, {icon: veloIcon}).addTo(map);
          break;
        case "b"://bus
        console.log(data);
            L.marker(data, {icon: busIcon}).addTo(map);
          break;
        case "t"://tram
        console.log(data);
            L.marker(data, {icon: tramIcon}).addTo(map);
          break;
        case "s"://suppression
          console.log(data);
              L.marker(data, {icon: tramIcon}).addTo(map);
          break;
        default:
          return;
      }
}
map.on('click', onMapClick);
document.addEventListener('keydown', onKeyDown);




var Stadia_OSMBright = L.tileLayer(
  "https://tiles.stadiamaps.com/tiles/osm_bright/{z}/{x}/{y}{r}.png",
  {
    maxZoom: 20,
    attribution:
      '&copy; <a href="https://stadiamaps.com/">Stadia Maps</a>, &copy; <a href="https://openmaptiles.org/">OpenMapTiles</a> &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors',
  }
);

Stadia_OSMBright.addTo(map);

var marker = L.marker([50.357113, 3.518332]).addTo(map);
/*
Gros Parkings en périphérie
Velos
Arrets de Bus
Arrets tram

gestion des formes (zones)
*/

// declaration des Icones :

var greenIcon = L.icon({
  iconUrl: '../assets/leaf-green.png',
  shadowUrl: '../assets/leaf-shadow.png',

  iconSize:     [38, 95], // size of the icon
  shadowSize:   [50, 64], // size of the shadow
  iconAnchor:   [22, 94], // point of the icon which will correspond to marker's location
  shadowAnchor: [4, 62],  // the same for the shadow
  popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
});
var parkingIcon = L.icon({
  iconUrl: '../assets/parking.png',


  iconSize:     [38, 38], // size of the icon
  iconAnchor:   [0,38], // point of the icon which will correspond to marker's location
  popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
});

var tramIcon = L.icon({
iconUrl: '../assets/tram.png',


iconSize:     [38, 38], // size of the icon
iconAnchor:   [0,38], // point of the icon which will correspond to marker's location
popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
});

var defaultIcon = L.icon({
iconUrl: '../assets/iconeDefaut.png',


iconSize:     [38, 60], // size of the icon
  iconAnchor:   [22, 60], // point of the icon which will correspond to marker's location
  popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
});

var veloIcon = L.icon({
iconUrl: '../assets/velo.png',


iconSize:     [38, 38], // size of the icon
iconAnchor:   [0,38], // point of the icon which will correspond to marker's location
popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
});

var busIcon = L.icon({
iconUrl: '../assets/bus.png',


iconSize:     [38, 38], // size of the icon
iconAnchor:   [0,38], // point of the icon which will correspond to marker's location
popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
});